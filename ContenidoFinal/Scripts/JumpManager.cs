﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpManager : MonoBehaviour {

    private bool jumpEnabled;

	public bool canJump(){
        return this.jumpEnabled;
    }

    void OnTriggerEnter2D(Collider2D collision){
        this.jumpEnabled = true;
    }

    void OnTriggerExit2D(Collider2D collision){
        this.jumpEnabled = false;
    }

    void Update(){
        this.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
    }
}
