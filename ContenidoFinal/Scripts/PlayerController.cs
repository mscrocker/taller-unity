﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float aceleration;
    public Vector2 maxSpeed;
    public float jumpForce;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private bool canJump(){
        JumpManager manager = this.transform.GetChild(1).GetComponent<JumpManager>();
        return manager.canJump();
    }

    private void FixedUpdate(){
        Vector2 force = new Vector2();
        Rigidbody2D body = this.gameObject.GetComponent<Rigidbody2D>();
        Vector2 currentVelocity = body.velocity;

        if ((Mathf.Abs(currentVelocity.x) < maxSpeed.x) ||  //Si la velocidad no paso el limite
            (currentVelocity.x * Input.GetAxis("Horizontal") < 0.0f) ){ // O se está intentando mover en dirección contraria
            force.x = Input.GetAxis("Horizontal") * aceleration; //Aplico la fuerza
        }

        if ((Input.GetAxis("Vertical") > 0.0f) && (this.canJump())){ //Si se está pulsando la tecla y puede saltar
            force.y = jumpForce; //Aplico la fuerza
        }
        this.gameObject.GetComponent<Rigidbody2D>().AddForce(force); //Aplico cambios

        body.velocity = new Vector2( //Evito que la velocidad se dispare
            Mathf.Clamp(body.velocity.x, -maxSpeed.x, maxSpeed.x),
            Mathf.Clamp(body.velocity.y, -maxSpeed.y, maxSpeed.y)
        );

    }

    private void OnCollisionEnter2D(Collision2D collision){
        if (collision.collider.gameObject.CompareTag("Lava")){
            this.transform.GetChild(2).GetComponent<Canvas>().enabled = true;
            Time.timeScale = 0.0f;
        }
    }

    public void restartGame(){
        Time.timeScale = 1.0f;
        this.transform.GetChild(2).GetComponent<Canvas>().enabled = false;
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
}
