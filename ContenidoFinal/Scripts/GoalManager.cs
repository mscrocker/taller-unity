﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalManager : MonoBehaviour {

	public void endGame(){
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif

    }

    private void OnTriggerEnter2D(Collider2D other){


        transform.GetChild(0).GetComponent<Canvas>().enabled = true;
    }
}
