﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private Quaternion rotation;

	void Awake () {
        this.rotation = this.transform.rotation;
	}
	

	void LateUpdate () {
        this.transform.rotation = this.rotation;
	}
}
