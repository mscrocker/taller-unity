# Taller de iniciación a Unity
Con motivo de la próxima Ludum Dare, ofrecemos un taller de introducción a Unity. En este taller intentaremos iniciar a todos aquellos que lo deseen para que puedan empezar a experimentar con el motor gráfico.

### Requisitos
Podeis asistir al taller sin  necesidad de traer nada y se proyectará todo lo que vayamos haciendo. Además, si quereis, podeis traer vuestros portátiles e ir siguiendo el taller paso a paso para ir construyendo un proyecto de juguete según avanzamos. Para ello, lo recomendable sería que trajerais los siguientes programas instalados.

  - Unity 5  (recomendada version 2018.2.7f1)
  - Un IDE para programar compatible con Unity. Recomendaciones:
	  - Visual Studio (sólo en Windows)
	  - Visual Studio Code (compatible con Windows y Linux)

Para el taller se proyectará utilizando la versión recomendada de Unity y Visual Studio Code.

### Contenido
En el taller se intentará realizar un proyecto corto de inicio a fin en el que se tocarán los siguientes temas:

  - Nociones básicas de motores gráficos
  - Prefabs en Unity
  - Importar y usar sprites en Unity
  - Movimiento por scripts
  - Creación de objetos con físicas y colisiones
  - Creación de un HUD
  - Empaquetado del proyecto